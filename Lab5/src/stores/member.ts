import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Member } from '@/typs/Member'

export const useMemberStore = defineStore('member', () => {
    const members = ref<Member[]>([
        { id: 1, name: "คนที่ 1", tel: "0812345678" },
        { id: 2, name: "คนที่ 2", tel: "0812345677" }

    ])
    const currentMember = ref<Member | null>()
    const searchMember = (tel: string) => {
        const index = members.value.findIndex((item) => item.tel === tel)
        if (index < 0) {
            currentMember.value = null
        }
        currentMember.value = members.value[index]
    }
    function clear() {

        currentMember.value = null
    }

    return {
        members, currentMember,
        searchMember,clear
    }
})
