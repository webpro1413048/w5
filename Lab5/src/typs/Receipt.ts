import type { Member } from "./Member";
import type { ReceiptItem } from "./Receiptitem";
import type { User } from "./User";

type Receipt = {
    id: number;
    createDate: Date;
    totalBefore:number,
    memberDiscount:number,
    total: number;
    receivedAmount: number;
    change: number;
    paymentType: string;
    userId: number;
    user?: User;
    memberId: number;
    member?: Member;
    receiptItems?: ReceiptItem[]
    
}

export type { Receipt }