type gender = 'male' | 'female' | 'others'
type roles = 'admin' | 'user'

type User = {
    id: number
    email: string
    password: string
    fullName: string
    gender: string // male,femail,others
    roles: string[] // addmin,user
  }
  export type {gender,User,roles}